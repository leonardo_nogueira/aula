<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /** if($request->has('title')){
            echo 'Tem título: '.$request->title;

        dd($request->only(['title','slug']));

        dd($request->except(['title','slug']));

        dd($request->input('date','28-05-2021'));

        return response('Resposta ok',200);

           return response('{Resposta ok}',200)->header('Content-Type','application/json');

           return response('{Resposta ok}',200)->cookie('autorizado','ok',6);

           return redirect('/');

         return redirect()->route('principal');

       return back();

        */
        $tabela=New Tabela();
        $tabela->title =$request.title;
        $tabela->description=$request.description;
        $tabela->content=$request.content;
        $tabela->slug=$request.slug;
        $tabela->save();
        return back()->withInput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
