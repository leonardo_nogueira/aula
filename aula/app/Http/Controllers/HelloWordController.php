<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloWordController extends Controller
{
    function __invoke(){

        return view('welcome',['name' => 'Joaozinho']);

    }
}
