
<form action="{{route('posts.store')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="title">Título:</label>
        <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">

    </div>

    <div class="form-group">
        <label for="description">Descrição:</label>
        <input type="text" name="description" id="description" class="form-control" value="{{old('description')}}">
    </div>
    <div class="form-group">
        <label for="content">Conteúdo</label>
        <textarea name="content" cols="30" rows="10" id="content" class="form-control">{{old('content')}}</textarea>

    </div>
    <div class="form-group">
        <label for="slug">Slug:</label>
        <input type="text" name="slug" id="slug" class="form-control" value="{{old('slug')}}">
    </div>

    <button class="btn btn-lg btn-success" type="submit">Criar Postagem</button>

</form>
